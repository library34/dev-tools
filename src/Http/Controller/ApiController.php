<?php

declare(strict_types=1);

namespace Clickable\DevTools\Http\Controller;

use Clickable\DevTools\Core\Exception\ApiException;
use Clickable\DevTools\Http\Request\RequestDTOInterface;
use Clickable\DevTools\Http\Request\RequestValidator;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

abstract class ApiController extends AbstractController
{
    public function __construct(private RequestValidator $requestValidator) {}

    protected function getRequestValidator(): RequestValidator
    {
        return $this->requestValidator;
    }

    protected function validateRequest(RequestDTOInterface $requestDTO): void
    {
        $errors = $this->requestValidator->validateRequest($requestDTO);
        if ($errors->count() > 0) {
            throw ApiException::make('Validation Error', $errors);
        }
    }
}