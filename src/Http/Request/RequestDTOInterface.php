<?php

declare(strict_types=1);

namespace Clickable\DevTools\Http\Request;

use Symfony\Component\HttpFoundation\Request;

interface RequestDTOInterface
{
    public function __construct(Request $request);

    public function getOriginalRequest(): Request;
}