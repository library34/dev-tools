<?php

declare(strict_types=1);

namespace Clickable\DevTools\Http\Request;

use Clickable\DevTools\Core\Error\Abbreviation;
use Clickable\DevTools\Core\Error\Error;
use Clickable\DevTools\Core\Error\ErrorCode;
use Clickable\DevTools\Core\Error\ErrorCollection;
use Clickable\DevTools\Core\Error\ErrorMessage;
use Clickable\DevTools\Core\Exception\InvalidEnumException;
use Symfony\Component\Validator\ConstraintViolationInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class RequestValidator
{
    private ValidatorInterface $validator;

    public function __construct(ValidatorInterface $validator)
    {
        $this->validator = $validator;
    }

    public function validateRequest(RequestDTOInterface $requestDTO): ErrorCollection
    {
        $errors = $this->validator->validate($requestDTO);
        $responseErrors = new ErrorCollection();
        if ($errors->count() > 0) {
            /** @var ConstraintViolationInterface $error */
            foreach ($errors as $error) {
                try {
                    $errorCode = ErrorCode::make((int)$error->getCode());
                    $responseErrors->add(new Error(
                        $errorCode,
                        Abbreviation::make('validation_error'),
                        ErrorMessage::make($error->getMessage())
                    ));
                } catch (InvalidEnumException) {
                    $responseErrors->add(new Error(
                        ErrorCode::make(ErrorCode::BAD_REQUEST),
                        Abbreviation::make('validation_error'),
                        ErrorMessage::make($error->getMessage())
                    ));
                }
            }
        }
        return $responseErrors;
    }
}