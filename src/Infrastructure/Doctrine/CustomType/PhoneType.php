<?php

declare(strict_types=1);

namespace Clickable\DevTools\Infrastructure\Doctrine\CustomType;

use Doctrine\DBAL\Platforms\AbstractPlatform;
use Doctrine\DBAL\Types\Type;
use Clickable\DevTools\Core\ValueObject\Phone;

class PhoneType extends Type
{
    private const NAME = 'phone';

    public function getSQLDeclaration(array $column, AbstractPlatform $platform): string
    {
        return 'VARCHAR(50)';
    }

    public function getName(): string
    {
        return self::NAME;
    }

    public function convertToPHPValue($value, AbstractPlatform $platform): Phone
    {
        return Phone::fromString($value);
    }

    public function convertToDatabaseValue($value, AbstractPlatform $platform): string
    {
        /** @var Phone $value */
        return $value->getNumber();
    }
}