<?php

declare(strict_types=1);

namespace Clickable\DevTools\Infrastructure\Validation\Path;

use Clickable\DevTools\Core\ValueObject\Exception\InvalidPathException;
use Clickable\DevTools\Core\ValueObject\Path;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use Symfony\Component\Validator\Exception\UnexpectedTypeException;

class PathConstraintValidator extends ConstraintValidator
{
    public function validate(mixed $value, Constraint $constraint): void
    {
        if (!$constraint instanceof PathConstraint) {
            throw new UnexpectedTypeException($constraint, PathConstraint::class);
        }
        try {
            Path::make((string)$value);
        } catch (InvalidPathException $e) {
            $this->context->buildViolation($e->getMessage())
                ->setCode((string)$e->getCode())
                ->addViolation();
        }
    }
}