<?php

declare(strict_types=1);

namespace Clickable\DevTools\Infrastructure\Validation\Path;

use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 */
#[\Attribute]
class PathConstraint extends Constraint
{

}