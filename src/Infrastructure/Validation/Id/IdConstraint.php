<?php

declare(strict_types=1);

namespace Clickable\DevTools\Infrastructure\Validation\Id;

use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 */
#[\Attribute]
class IdConstraint extends Constraint
{

}