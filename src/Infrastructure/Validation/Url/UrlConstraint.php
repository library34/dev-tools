<?php

declare(strict_types=1);

namespace Clickable\DevTools\Infrastructure\Validation\Url;

use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 */
#[\Attribute]
class UrlConstraint extends Constraint
{

}