<?php

declare(strict_types=1);

namespace Clickable\DevTools\Infrastructure\Validation\Phone;

use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 */
#[\Attribute]
class PhoneConstraint extends Constraint
{

}