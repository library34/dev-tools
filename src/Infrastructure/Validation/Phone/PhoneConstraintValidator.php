<?php

declare(strict_types=1);

namespace Clickable\DevTools\Infrastructure\Validation\Phone;

use Clickable\DevTools\Core\Exception\InvalidPhoneNumberException;
use Clickable\DevTools\Core\ValueObject\Phone;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use Symfony\Component\Validator\Exception\UnexpectedTypeException;

class PhoneConstraintValidator extends ConstraintValidator
{
    public function validate(mixed $value, Constraint $constraint): void
    {
        if (!$constraint instanceof PhoneConstraint) {
            throw new UnexpectedTypeException($constraint, PhoneConstraint::class);
        }
        try {
            Phone::fromString((string)$value);
        } catch (InvalidPhoneNumberException $e) {
            $this->context->buildViolation($e->getMessage())
                ->setCode((string)$e->getCode())
                ->addViolation();
        }
    }
}