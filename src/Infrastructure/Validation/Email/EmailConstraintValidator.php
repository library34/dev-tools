<?php

declare(strict_types=1);

namespace Clickable\DevTools\Infrastructure\Validation\Email;

use Clickable\DevTools\Core\ValueObject\Email;
use Clickable\DevTools\Core\ValueObject\Exception\InvalidEmailException;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use Symfony\Component\Validator\Exception\UnexpectedTypeException;

class EmailConstraintValidator extends ConstraintValidator
{
    public function validate(mixed $value, Constraint $constraint): void
    {
        if (!$constraint instanceof EmailConstraint) {
            throw new UnexpectedTypeException($constraint, EmailConstraint::class);
        }
        try {
            Email::make((string)$value);
        } catch (InvalidEmailException $e) {
            $this->context->buildViolation($e->getMessage())
                ->setCode((string)$e->getCode())
                ->addViolation();
        }
    }
}