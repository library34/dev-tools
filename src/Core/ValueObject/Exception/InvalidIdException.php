<?php

declare(strict_types=1);

namespace Clickable\DevTools\Core\ValueObject\Exception;

use Clickable\DevTools\Core\Error\ErrorCode;
use Clickable\DevTools\Core\Exception\ValidationException;
use Clickable\DevTools\Core\ValueObject\Id;

class InvalidIdException extends ValidationException
{
    public static function lessThanMin(): static
    {
        return new static(
            sprintf('Id can\'t be less than %s', Id::MIN_VALUE),
            ErrorCode::BAD_REQUEST
        );
    }
}