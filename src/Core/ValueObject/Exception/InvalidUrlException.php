<?php

declare(strict_types=1);

namespace Clickable\DevTools\Core\ValueObject\Exception;

use Clickable\DevTools\Core\Error\ErrorCode;
use Clickable\DevTools\Core\Exception\ValidationException;

class InvalidUrlException extends ValidationException
{
    public static function emptyUrl(): static
    {
        return new static('Url can\'t be empty.', ErrorCode::BAD_REQUEST);
    }

    public static function invalidFormat(string $url): static
    {
        return new static(
            sprintf('Invalid url format. Got url "%s".', $url),
            ErrorCode::BAD_REQUEST
        );
    }
}