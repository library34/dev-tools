<?php

namespace Clickable\DevTools\Core\ValueObject\Exception;

use Clickable\DevTools\Core\Error\ErrorCode;
use Clickable\DevTools\Core\Exception\ValidationException;

class InvalidPathException extends ValidationException
{
    public static function emptyPath(): static
    {
        return new static('Path can\'t be empty.', ErrorCode::BAD_REQUEST);
    }

    public static function noFileOrDirFound(string $path): static
    {
        return new static(
            sprintf('No directory or file found in the resulting path. Got path "%s".', $path),
            ErrorCode::BAD_REQUEST
        );
    }

    public static function notFile(): static
    {
        return new static('Path mus be a file.', ErrorCode::BAD_REQUEST);
    }

    public static function notDir(): static
    {
        return new static('Path must be a directory.', ErrorCode::BAD_REQUEST);
    }
}