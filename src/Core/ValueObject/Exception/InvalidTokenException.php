<?php

declare(strict_types=1);

namespace Clickable\DevTools\Core\ValueObject\Exception;

use Clickable\DevTools\Core\Error\ErrorCode;
use Clickable\DevTools\Core\Exception\ValidationException;

class InvalidTokenException extends ValidationException
{
    public static function emptyToken(): static
    {
        return new static('Token can\'t be empty.', ErrorCode::BAD_REQUEST);
    }
}