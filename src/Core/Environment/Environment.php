<?php

declare(strict_types=1);

namespace Clickable\DevTools\Core\Environment;

use Clickable\DevTools\Core\ValueObject\Enum;

class Environment extends Enum
{
    public const DEV = 'dev';
    public const TEST = 'test';
    public const PROD = 'prod';
}