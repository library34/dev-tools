<?php

declare(strict_types=1);

namespace Clickable\DevTools\Core\Generator;

/**
 * @template TKey
 * @template TValue
 * @implements \IteratorAggregate<TKey, TValue>
 */
final class RewindableGenerator implements \IteratorAggregate
{
    /**
     * @param callable(): \Generator<TKey, TValue> $generatorFunction
     */
    public function __construct(private $generatorFunction) {}

    /**
     * @inheritDoc
     */
    public function getIterator(): \Traversable
    {
        return ($this->generatorFunction)();
    }
}