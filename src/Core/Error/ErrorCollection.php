<?php

declare(strict_types=1);

namespace Clickable\DevTools\Core\Error;

use Clickable\DevTools\Core\Collection\Collection;

class ErrorCollection extends Collection
{
    protected ?string $allowedType = Error::class;

    public static function fromScalarErrorsArray(array $errorsArray): static
    {
        $errors = new static();
        foreach ($errorsArray as $errorData) {
            $error = Error::fromArray($errorData);
            $errors->add($error);
        }

        return $errors;
    }

    public function asArray(): array
    {
        return $this->map(static function (Error $error) {
            return $error->asArray();
        });
    }
}