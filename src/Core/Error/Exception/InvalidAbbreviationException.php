<?php

declare(strict_types=1);

namespace Clickable\DevTools\Core\Error\Exception;

use Clickable\DevTools\Core\Error\Abbreviation;
use Clickable\DevTools\Core\Error\ErrorCode;
use Clickable\DevTools\Core\Exception\ValidationException;

class InvalidAbbreviationException extends ValidationException
{
    public static function emptyAbbreviation(): static
    {
        return new static('Error abbreviation can\'t be empty.', ErrorCode::BAD_REQUEST);
    }

    public static function invalidFormat(string $gotAbbreviation): static
    {
        $errorMessage = sprintf(
            'Abbreviation must satisfy "%s" pattern. Got abbreviation "%s"',
            Abbreviation::ABBREVIATION_PATTERN,
            $gotAbbreviation
        );

        return new static($errorMessage, ErrorCode::BAD_REQUEST);
    }
}