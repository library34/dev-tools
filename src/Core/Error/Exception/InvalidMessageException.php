<?php

declare(strict_types=1);

namespace Clickable\DevTools\Core\Error\Exception;

use Clickable\DevTools\Core\Error\ErrorCode;
use Clickable\DevTools\Core\Error\ErrorMessage;
use Clickable\DevTools\Core\Exception\ValidationException;

class InvalidMessageException extends ValidationException
{
    public static function emptyMessage(): static
    {
        return new static('Error message can\'t be empty.', ErrorCode::BAD_REQUEST);
    }

    public static function greaterThanMax(string $gotMessage): static
    {
        $gotMessageLength = mb_strlen($gotMessage);
        $errorMessage = sprintf(
            'Error message can\'t be greater than %s. Got with length %s',
            ErrorMessage::MAX_LENGTH,
            $gotMessage
        );

        return new static($errorMessage, ErrorCode::BAD_REQUEST);
    }
}