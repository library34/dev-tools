<?php

declare(strict_types=1);

namespace Clickable\DevTools\Core\Exception;

class RuntimeException extends \RuntimeException
{

}