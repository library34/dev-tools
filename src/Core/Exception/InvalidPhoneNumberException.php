<?php

declare(strict_types=1);

namespace Clickable\DevTools\Core\Exception;

use Clickable\DevTools\Core\Error\ErrorCode;

class InvalidPhoneNumberException extends ValidationException
{
    public static function emptyPhone(): static
    {
        return new static('Phone number can\'t be empty.', ErrorCode::BAD_REQUEST);
    }

    public static function invalidFormat(string $phone): static
    {
        return new static(
            sprintf('Invalid phone format. Got phone "%s".', $phone),
            ErrorCode::BAD_REQUEST
        );
    }
}